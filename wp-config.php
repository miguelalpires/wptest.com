<?php
/**
 * A configuração de base do WordPress
 *
 * Este ficheiro define os seguintes parâmetros: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, e ABSPATH. Pode obter mais informação
 * visitando {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} no Codex. As definições de MySQL são-lhe fornecidas pelo seu serviço de alojamento.
 *
 * Este ficheiro é usado para criar o script  wp-config.php, durante
 * a instalação, mas não tem que usar essa funcionalidade se não quiser.
 * Salve este ficheiro como "wp-config.php" e preencha os valores.
 *
 * @package WordPress
 */

// ** Definições de MySQL - obtenha estes dados do seu serviço de alojamento** //
/** O nome da base de dados do WordPress */
define('DB_NAME', 'localwp');

/** O nome do utilizador de MySQL */
define('DB_USER', 'wpuser');

/** A password do utilizador de MySQL  */
define('DB_PASSWORD', '12345');

/** O nome do serviddor de  MySQL  */
define('DB_HOST', 'localhost');

/** O "Database Charset" a usar na criação das tabelas. */
define('DB_CHARSET', 'utf8mb4');

/** O "Database Collate type". Se tem dúvidas não mude. */
define('DB_COLLATE', '');

/**#@+
 * Chaves Únicas de Autenticação.
 *
 * Mude para frases únicas e diferentes!
 * Pode gerar frases automáticamente em {@link https://api.wordpress.org/secret-key/1.1/salt/ Serviço de chaves secretas de WordPress.org}
 * Pode mudar estes valores em qualquer altura para invalidar todos os cookies existentes o que terá como resultado obrigar todos os utilizadores a voltarem a fazer login
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'HL9#6<WL|{$|2p2T]4pQ#YZ*uE}:ugm_+|W(yVJ7<-.^+0#[x+Wr`SNITc5zwaon');
define('SECURE_AUTH_KEY',  '}@aWt-T8 1!E|h%!!EoHN0f$?-NEWLv|JWm{@MDn+<87|J0<.lHE>w^f+L9?Dy3N');
define('LOGGED_IN_KEY',    'mYK>C4qUlG#fq7+9LkU7uWF->&mE|i5!Q292Zb2;@g+0/({(vbKX/7I+#D-.=K.N');
define('NONCE_KEY',        '*3(e)uf$sximAJHmDGdO-ZA)|CCe_/`{KBCiX-D8+/9pGd+7kaXE!_O5jp+:+_ZI');
define('AUTH_SALT',        ';NY*t1Zo $qW=zF2ZZwiWhLzt:#m+=q:e-^^ey]SuWXG|hhD4.a^aC-u+32&%xGU');
define('SECURE_AUTH_SALT', 'Vo0s/3=s2U|y[A@7=)K*tb&&n}e.uA6D8OpXr6->z45F6{DAQ,W?N$9WvxFyO%IB');
define('LOGGED_IN_SALT',   '-+x9/7w1[kOif>yOb6sGPUfKd9rD;+R{BBY;pW-sDDKpw)o=E@Pf?vw}#E.59+A)');
define('NONCE_SALT',       ',]L%&MHK FFf!$i,4x<q4w}862H5dvGdG(+64s;+7Gz.[ 0oqy>u;EC!.},:-3`D');

/**#@-*/

/**
 * Prefixo das tabelas de WordPress.
 *
 * Pode suportar múltiplas instalações numa só base de dados, ao dar a cada
 * instalação um prefixo único. Só algarismos, letras e underscores, por favor!
 */
$table_prefix  = 'wp_';

/**
 * Para developers: WordPress em modo debugging.
 *
 * Mude isto para true para mostrar avisos enquanto estiver a testar.
 * É vivamente recomendado aos autores de temas e plugins usarem WP_DEBUG
 * no seu ambiente de desenvolvimento.
 */
define('WP_DEBUG', false);

/* E é tudo. Pare de editar! */

/** Caminho absoluto para a pasta do WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Define as variáveis do WordPress e ficheiros a incluir. */
require_once(ABSPATH . 'wp-settings.php');
