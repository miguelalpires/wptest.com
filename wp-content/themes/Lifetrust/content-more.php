<?php
/**
 * Template for displaying more buttons on custom post type entries
 */
?>
<a href="<?php the_field('more_link') ?>" class="btn <?php the_field('more_text') ?>">
	<?php the_field('more_text') ?>
</a>
