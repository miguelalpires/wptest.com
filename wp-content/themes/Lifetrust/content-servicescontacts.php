
<?php
/**
 * Template for displaying services contacts cotum post type
 */
?>
<div class="contactbox">
	
	<div class="imgsection">
		<img src="<?php the_field('image') ?>" alt="<?php the_title()?>" />
	</div>
	
	<div class="contacts">
		
		<div class="title"><?php the_title()?></div>

		<?php get_template_part( 'content', 'contact'); ?>

		<div class="btnsection">
			<?php get_template_part( 'content', 'more'); ?>
		</div>

	</div>
</div>
