<?php
/**
 * The template for displaying the header
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">

<head>
	
	<meta charset="utf-8">
	<!--[if IE]><![endif]-->
	<!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge;chrome=1"><![endif]-->
	<title>Lifetrust - Homepage</title>
	<meta name="description" content="">
	<meta name="keywords" content="" />
	<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;">
	<link rel="shortcut icon" href="/favicon.ico">
	<link rel="apple-touch-icon" href="/apple-touch-icon.png">
	
	<?php wp_head(); ?>
	
	<!--[if lt IE 9]>
	<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/html5.js"></script>
	<![endif]-->
</head>

<body>

<?php get_sidebar(); ?>

</div><!-- .sidebar -->

<?php
	if(is_front_page()){
		get_template_part( 'content', 'mainbanner' );
	} else {
		get_template_part( 'content', 'banner' );
	}
?>
