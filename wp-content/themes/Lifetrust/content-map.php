<?php
/**
 * Template for displaying maps custom post type entries
 */
?>	
<div class="map">
	<a href="<?php the_field('map_link')?>" target="_blank">
		<img src="<?php the_field('map_image')?>" alt="lifetrust" />
	</a>
</div>

<div class="contactholder">

	<p><?php the_field('short_description')?></p>
	<ul class="address clearfix">

		<?php while( have_rows('contacts', '') ): $contact = the_row(); ?>

			<li>
				<?php if(!empty($contact->short_description)):?>
					<h2 class="title"><?php echo $contact->short_description;?></h2>
				<?php endif;?>

				<?php if(!empty($contact->address)) :?>
					<address> <?php echo $contact->address;?></address>
				<?php endif;?>

				<dl>
					<?php if(!empty($contact->email)) :?>
						<dt>Email:</dt><dd><?php echo $contact->email;?></dd>
					<?php endif;?>
					<?php if(!empty($contact->telephone)) :?>
						<dt>Tel:</dt><dd><?php echo $contact->telephone;?></dd>
					<?php endif;?>
					<?php if(!empty($contact->fax)) :?>
						<dt>Fax:</dt><dd><?php echo $contact->fax;?></dd>
					<?php endif;?>
				</dl>
			</li>
		<?php endwhile; ?>
	</ul>
