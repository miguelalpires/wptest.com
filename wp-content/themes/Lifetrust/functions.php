<?php

	//Action hooks
	
	function lifetrust_script_enqueue(){

		wp_enqueue_style('customstyle', get_template_directory_uri() . '/css/styles.css', array(), '1', 'all');
		wp_enqueue_script('customjs', get_template_directory_uri() . '/js/modernizr-1.6.min.js', array(), '', false);
		wp_enqueue_script('customjs', get_template_directory_uri() . '/js/jquery.js', array(), '', false);
		wp_enqueue_script('customjs', get_template_directory_uri() . '/js/script.js', array(), '1', true);	
		
	}

	add_action('wp_enqueue_scripts', 'lifetrust_script_enqueue');


	function lifetrut_theme_setup(){
		
		add_theme_support('menus');		
			
		register_nav_menu('primary','Primary Header Navigation');
		register_nav_menu('services','Services Navigation');
		register_nav_menu('contacts','Contacts Navigation');
	}

	add_action('after_setup_theme', 'lifetrut_theme_setup');

	//Filter hooks

	function my_wpcf7_form_elements($html) {
		$text = '(Select One)';
		$html = str_replace('<option value="">---</option>', '<option value="">' . $text . '</option>', $html);
		return $html;
	}

	add_filter('wpcf7_form_elements', 'my_wpcf7_form_elements');

	// Theme support

	add_theme_support('post-formats');
?>
