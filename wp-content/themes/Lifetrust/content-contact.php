<?php
/**
 * Template for displaying contact custom post type entries
 */
?>	
	
	<?php $contact = get_field('contact'); ?>
	
	<?php if(!empty($contact->short_description)):?>
		<h2 class="title"><?php echo $contact->short_description;?></h2>
	<?php endif;?>

	<?php if(!empty($contact->address)) :?>
		<address> <?php echo $contact->address;?></address>
	<?php endif;?>

	<dl>
		<?php if(!empty($contact->email)) :?>
			<dt>Email:</dt><dd><?php echo $contact->email;?></dd>
		<?php endif;?>
		<?php if(!empty($contact->telephone)) :?>
			<dt>Tel:</dt><dd><?php echo $contact->telephone;?></dd>
		<?php endif;?>
		<?php if(!empty($contact->fax)) :?>
			<dt>Fax:</dt><dd><?php echo $contact->fax;?></dd>
		<?php endif;?>
	</dl>
