<?php
/**
 * Template for displaying banner on header of front page
 */
?>	
<div class="mainbanner">
	<div class="shadow">
		<div class="bannerholder clearfix">
			<img src="<?php echo get_template_directory_uri() ?>/images/mainbanner.jpg" alt="" />
			<div class="textsection">
				<table>
					<tr>
						<td>
							<div class="text">Serving Customers Since 2002</div>
							<h1 class="title"><strong>Lifetrust</strong> <span>strives to build <br /> long term relationships and</span><br /> considers each investor as a partner.</h1>
							<div class="clearfix">
								<a href="#link" class="btn invest">Invest as a partner</a>
								<a href="#link" class="btn contactbig">Contact Us</a>
							</div>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</div>
</div>
