<?php
/**
 * Template Name: Services Page
 */

?>

<?php get_header();?>

<div id="primary" class="content-area">
	<div id="content" class="site-content" role="main">

			
<!-- Services types boxes -->
<article>
	<div class="shadow">
		<div class="contentholder">
			<div class="shadowcontentholder clearfix">
				<div class="content">

					<!-- Contact box -->

					<?php 
					    $args = array(
					        'post_type' => 'servicescontacts',
					        'orderby' => 'title',
					        'order' => 'ASC'
					    );
					    $the_query = new WP_Query( $args );		    
					?>
					<?php if ( have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>	

					<?php get_template_part( 'content', 'servicescontacts' ); ?>

					<?php endwhile; endif; ?>


					<!-- Services types boxes -->


					<div class="ohidden">
						<ul class="services">
							<?php 
							    $args = array(
							        'post_type' => 'service',
							        'orderby' => 'rank',
							        'order' => 'ASC'
							    );
							    $the_query = new WP_Query( $args );		    
							?>
							<?php if ( have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>	

							<?php get_template_part( 'content', 'service' ); ?>

							<?php endwhile; endif; ?>
						</ul>

					<div class="service"></div>
					</div>
				</div>

				<!-- Secondary menu -->
				<div class="sidebar">
					<ul>
						<?php wp_nav_menu(array('theme_location'=>'services'));?>
					</ul>
				</div>

			</div>
		</div>
	</div>
</article>		
<?php get_footer(); ?>
