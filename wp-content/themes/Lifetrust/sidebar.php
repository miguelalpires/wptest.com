<?php
/**
 * The sidebar containing the main menu area
 *
 */
?>
<div class="supernav">
	<div class="frame clearfix">
		<ul>
				<li><a href="#link">Industry Overview</a></li>
			<li><a href="#link">Sitemap</a></li>
		</ul>
	</div>
</div>

<nav class="mainnav">
	<div class="frame clearfix">
		<strong class="logo"><a href="#link"><img src="<?php echo get_template_directory_uri() ?>/images/logo.png" alt="Lifetrust" /></a></strong>
		<!--TODO: Add selected menu item -->
		<?php wp_nav_menu(array('theme_location'=>'primary'));?>
	</div>	
</nav>
