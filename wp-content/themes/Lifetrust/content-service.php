<?php
/**
 * Template for displaying service custom post type entries: Service
 */
?>	
<li>
	<h2 class="title clearfix">
		<img src="<?php the_field('image'); ?>" alt="<?php the_title(); ?>" />
		<?php the_title(); ?>
	</h2>
	<div class="desc"><?php the_field('description'); ?></div>
	<div class="btnsection clearfix"><a href="<?php the_field('more') ?>" class="btn moresmall">more</a></div>
</li>
