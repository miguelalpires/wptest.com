<?php
/**
 * Template Name: About Page
 */

get_header(); ?>

<article>
	<div class="shadow">
		<div class="homecontent">
			<div class="homeboxes clearfix">

				<!-- homeboxes and homecontacts type boxes -->

				<?php 
				    $args = array(
				        'post_type' => array('homebox', 'homecontact'),
				        'orderby' => 'rank',
				        'order' => 'ASC'
				    );
				    $the_query = new WP_Query( $args );		    
				?>
				<?php if ( have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>	

					<?php get_template_part( 'content', get_post_type()); ?>	

				<?php endwhile; endif; ?>

			</div>		
		</div><!-- #content -->
	</div>
</article>
<?php get_footer(); ?>
