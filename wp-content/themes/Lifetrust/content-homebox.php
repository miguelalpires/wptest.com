<?php
/**
 * Template for displaying homebox custom post type entries
 */
?>	
<div class="homebox">
		
	<div class="imgsection"><img  src="<?php the_field('image') ?>" alt="<?php the_title() ?>"/></div>
	<h2 class="title"><?php the_title()?></h2>
	<div class="desc"><?php the_field('short_description')?></div>

	<ul>
		<?php while( have_rows('items', '') ): $item = the_row(); ?>
			<li><span><?php echo $item->name; ?> </span></li>
		<?php endwhile; ?>
	</ul>

	<?php get_template_part( 'content', 'more'); ?>

</div>
