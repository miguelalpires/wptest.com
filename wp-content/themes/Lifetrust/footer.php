<?php
/**
 * The template for displaying the footer
 */
?>
</div><!-- .site-content -->

	<footer>
		<div class="frame">
			<div class="row clearfix">
				<?php wp_nav_menu(array('theme_location'=>'primary'));?>
				<div class="privacy"><a href="#link">Privacy Policy</a></div>
			</div>
			<div class="row clearfix">
				<div class="copy">&copy; 2010 LifeTrust. All Rights Reserved.</div>
				<div class="by"><a href="http://www.bluefountainmedia.com" target="_blank">Website Design</a> by <a href="http://www.bluefountainmedia.com/blog" target="_blank">Blue Fountain Media</a></div>
			</div>
		</div>
	</footer>

</div><!-- .site -->

<?php 
   /* Always have wp_footer() just before the closing </body>
    * tag of your theme, or you will break many plugins, which
    * generally use this hook to reference JavaScript files.
    */
    wp_footer(); 
?>
</body>
</html>
