<?php
/**
 * Template for displaying banner on header of generic pages
 */
?>	
<div class="mainbanner">
	<div class="shadow">
		<div class="bannerholder clearfix">
			<img src="<?php echo get_template_directory_uri() ?>/images/<?php echo $pagename ?>.jpg" alt="" />
			<div class="textsection">
				<table style="width:775px">
					<tr>
						<td>
							<h1 class="pagetitle"><?php echo $pagename ?></h1>
							<ul class="breadcrumbs">
								<li><a href="#link">Home</a></li>
								<li><?php echo $pagename ?></li>
							</ul>
						</td>
						<td>
							<blockquote>
								<div class="lalign">“our core value is that some mission statement</div>
								<div class="ralign">or testimonial goes here in this area...”</div>
							</blockquote>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</div>
</div>