<?php
/**
 * Template for displaying homecontact custom post type entries
 */
?>	
<div class="homebox">
		
	<div class="imgsection"><img style="margin-top:-19px;" src="<?php the_field('image') ?>" alt="<?php the_title() ?>"/></div>
	<h2 class="title"><?php the_title()?></h2>
	<div class="desc"><?php the_field('short_description')?></div>

	<?php get_template_part( 'content', 'contact'); ?>

	<?php get_template_part( 'content', 'more'); ?>
</div>
