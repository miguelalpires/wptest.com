<?php
/**
 * Template Name: Contact Page
 */

get_header(); ?>

	<article>
		<div class="shadow">
			<div class="contentholder">
				<div class="shadowcontentholder clearfix">
					<div class="content">
						
						<!-- Contacts map -->

						<?php 
						    $args = array(
						        'post_type' => 'map',
						    );
						    $the_query = new WP_Query( $args );		    
							?>
							<?php if ( have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>	
										
								<?php get_template_part( 'content', 'map' ); ?>

							<?php endwhile; endif; ?>

						<!-- Form -->	
						
						<div class="contactform">
							<?php echo do_shortcode( '[contact-form-7 id="99" title="Contact form 1"]' ); ?>
						</div>
						
					</div>
				</div>

				<!-- Contact Menu -->

				<div class="sidebar">
					<ul>
						<!-- TODO add class="active" on first <li> -->	
						<?php wp_nav_menu(array('theme_location'=>'contacts'));?>
					</ul>
				</div>
			</div>
		</div>
	</article>
<?php get_footer(); ?>
